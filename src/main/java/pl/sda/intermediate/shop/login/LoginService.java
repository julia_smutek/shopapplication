package pl.sda.intermediate.shop.login;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.sda.intermediate.shop.registration.User;
import pl.sda.intermediate.shop.registration.UserDAO;

@Service
public class LoginService {

    @Autowired
    @Qualifier(value = "DBUserDAO")
    private UserDAO userDAO;

    public boolean login(LoginDTO loginDTO) {

        boolean ableToLogin = userDAO
                .findUserByEmail(loginDTO.getLogin())
                .map( u -> compareHashes(loginDTO, u))
                .orElse(false);

        // powyżej
        // jesli przychodzi optional to map wykona się kiedy będzie true a jesli nie to wykona się or else

        if (ableToLogin) {
            UserContextHolder.addLoggedUser(loginDTO);
        }

        return ableToLogin;

    }

    private boolean compareHashes(LoginDTO loginDTO, User u) {
        return u.getPasswordHash().equals(DigestUtils.sha512Hex(loginDTO.getPassword()));
    }

}
