package pl.sda.intermediate.shop.registration;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class FileUserDAO implements UserDAO {

    private Map<String, User> usersByEmail = new HashMap<>();
    private String filePath = "C:\\Users\\julia\\Desktop\\shopApp\\src\\main\\resources\\pageDatabase";

    {
        this.readFromFile();
    }

    public void addNewUser(User user) {

        usersByEmail.put(user.getEmail(), user);

        try (
                FileOutputStream fos = new FileOutputStream(filePath);
                ObjectOutputStream oos = new ObjectOutputStream(fos)
        ) {

            oos.writeObject(usersByEmail);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public boolean checkIfUserExistsByEmail(String email) {
//        return usersByEmail.stream().anyMatch(u -> u.getEmail().equals(email));
        return usersByEmail.containsKey(email);

    }

    private void readFromFile() {

        try (
                FileInputStream fis = new FileInputStream(filePath);
                ObjectInputStream ois = new ObjectInputStream(fis)
        ) {

            Object o = ois.readObject();
            if(o instanceof List) {
                List<User> users = (List<User>) o;
                users.stream()
                        .collect(Collectors.toMap(u -> u.getEmail(), u -> u));
            } else {
                usersByEmail = (Map<String, User>) o;
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public Optional<User> findUserByEmail(String email) {
        return Optional.ofNullable(usersByEmail.get(email));
    }

    public String findUserCity(String email) {
        return "Lodz";
    }

}
