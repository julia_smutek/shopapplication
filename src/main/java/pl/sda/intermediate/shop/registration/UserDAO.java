package pl.sda.intermediate.shop.registration;

import java.util.Optional;

public interface UserDAO {
    void addNewUser(User user);
    boolean checkIfUserExistsByEmail(String email);
    Optional<User> findUserByEmail(String email);
}
