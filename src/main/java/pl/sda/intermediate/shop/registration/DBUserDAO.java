package pl.sda.intermediate.shop.registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional // dzięki temu nie muszę pisać getTransaction().begin() i getTransaction.commit()
public class DBUserDAO implements UserDAO {

    @Autowired
    EntityManager entityManager;

    @Override
    public void addNewUser(User user) {
        entityManager.persist(user.getAddress());
        entityManager.persist(user);
    }

    @Override
    public boolean checkIfUserExistsByEmail(String email) {
        List<User> usersWithSameEmails= entityManager
                .createQuery("FROM User u WHERE u.email = :mail", User.class)
                .setParameter("mail",email)
                .getResultList();
        return !usersWithSameEmails.isEmpty();
    }

    @Override
    public Optional<User> findUserByEmail(String email) {
        List<User> users= entityManager.createQuery("FROM User u WHERE u.email = :email",User.class)
                .setParameter("email",email)
                .getResultList();

                if(users.isEmpty()){
                    return Optional.empty();
                }
        return Optional.of(users.get(0));
    }
}
