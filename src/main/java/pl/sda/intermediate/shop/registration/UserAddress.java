package pl.sda.intermediate.shop.registration;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "addresses")
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserAddress implements Serializable {
    @Transient
    private static long serialVersionUID = 1223L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String city;
    private String country;
    @Column(name = "zip_code")
    private String zipCode;
    private String street;

}
